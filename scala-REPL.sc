//CONTROL STRUCTURES


//Looping with for and foreach

val a = Array("apple", "banana", "orange")

for (e <- a) println(e)


for (e <- a) {
   // imagine this requires multiple lines
   val s = e.toUpperCase
   println(s)
   }

//Returning values from a for loop


val newArray = for (e <- a) yield e.toUpperCase


//USING RANGE

for (i <- 1 to 3) println(i)

for (i <- 1 to 10 if i < 4) println(i)


//iterating maps

val names = Map("fname" -> "Robert",
  "lname" -> "Goren")
for ((k,v) <- names) println(s"key: $k, value: $v")


a.foreach(e => println(e.toUpperCase))



for {
  i <- 1 to 10
  if i != 1
  if i % 2 == 0
} println(i)

for {
  i <- 1 to 10
  if i % 2 == 0
} yield i


for (i <- 1 to 2; j <- 1 to 2) println(s"i = $i, j = $j")

for {
  i <- 1 to 3
  j <- 1 to 5
  k <- 1 to 10
} println(s"i = $i, j = $j, k = $k")


for {
  i <- 1 to 10
  if i > 3
  if i < 6
  if i % 2 == 0
} println(i)


//Creating a for Comprehension (for/yield Combination)


var names1 = Array("chris", "ed", "maurice")

val capNames = for (e <- names1) yield e.capitalize


val fruits = "apple" :: "banana" :: "orange" :: Nil

val out = for (e <- fruits) yield e.toUpperCase


//Implementing break and continue

package com.alvinalexander.breakandcontinue
import util.control.Breaks._
object BreakAndContinueDemo extends App {
  println("\n=== BREAK EXAMPLE ===")
  breakable {
    for (i <- 1 to 10) {
      println(i)
      if (i > 4) break // break out of the for loop
    }
  }






  Using the if Construct Like a Ternary Operator

  val absValue = if (a < 0) -a else a






  val month = i match {
    case 1 => "January"
    case 2 => "February"
    case 3 => "March"
    case 4 => "April"
    case 5 => "May"
    case 6 => "June"
    case 7 => "July"
    case 8 => "August"
    case 9 => "September"
    case 10 => "October"
    case 11 => "November"
    case 12 => "December"
    case _ => "Invalid month" // the default, catch-all
  }




  val i = 5
  i match {
    case 1 | 3 | 5 | 7 | 9 => println("odd")
    case 2 | 4 | 6 | 8 | 10 => println("even")
  }
