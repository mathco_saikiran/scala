val s = "Hello, world"
s.length // 12
val s1 = "Hello" + " world"

"hello".foreach(println)

for (c <- "hello") println(c)



s.getBytes.foreach(println)

val result = "hello world".filter(_ != 'l')

"scala".drop(2).take(2).capitalize

val a1 = "Hello"

val a2 = "Hello"

a1 == a2

a1.toUpperCase == a2.toUpperCase


//Creating Multiline Strings

val foo = """This is
a multiline
String"""

// Splitting Strings

"hello world".split(" ")

"hello world".split(" ").foreach(println)

val p = "eggs, milk, butter, Coco Puffs"

p.split(",")

s.split(",").map(_.trim)


val name = "Fred"
val age = 33
val weight = 200.00

println(s"$name is $age years old, and weighs $weight pounds.")
println(s"Age next year: ${age + 1}")
println(s"You are 33 years old: ${age == 33}")


val upper = "hello, world".map(c => c.toUpper)

val upper2 = "hello, world".map(_.toUpper)

val upper3 = "hello, world".filter(_ != 'l').map(_.toUpper)

val upper4 = for (c <- "hello, world") yield c.toUpper

"hello".getBytes

"hello".getBytes.foreach(println)

//Finding Patterns in Strings


val numPattern = "[0-9]+".r

val address = "123 Main Street Suite 101"

val match1 = numPattern.findFirstIn(address)

var matches = numPattern.findAllIn(address)

matches.foreach(println)

var matches1 = numPattern.findAllIn(address).toArray



import scala.util.matching.Regex

val numPattern1 = new Regex("[0-9]+")

val address1 = "123 Main Street Suite 101"

val match2 = numPattern.findFirstIn(address)

val result = numPattern.findFirstIn(address).getOrElse("no match")

numPattern.findFirstIn(address).foreach { e =>
  // perform the next step in your algorithm,
  // operating on the value 'e'
}

match1 match {
  case Some(s) => println(s"Found: $s")
  case None =>
}

//Replacing Patterns in Strings


val address = "123 Main Street".replaceAll("[0-9]", "x")

val regex = "[0-9]".r

val newAddress = regex.replaceAllIn("123 Main Street", "x")

val result = "123".replaceFirst("[0-9]", "x")

val regex = "H".r

val result = regex.replaceFirstIn("Hello world", "J")

//Extracting Parts of a String That Match Patterns


val pattern = "([0-9]+) ([A-Za-z]+)".r

val pattern(count, fruit) = "100 Bananas"

"hello".charAt(0)

"hello"(0)

//------------------------------------------------------------------


//For an immutable sequence of objects that share the same type you can
//use Scala’s List class.

val oneTwo = List(1, 2)
val threeFour = List(3, 4)
val oneTwoThreeFour = oneTwo ::: threeFour
println(""+ oneTwo +" and "+ threeFour +" were not mutated.")
println("Thus, "+ oneTwoThreeFour +" is a new list.")

val oneTwoThree = 1 :: 2 :: 3 :: Nil
println(oneTwoThree)

val thrill = "Will" :: "fill" :: "until" :: Nil

thrill(2)

thrill.count(s => s.length == 4)

thrill.drop(2)
thrill.dropRight(2)
thrill.exists(s => s == "until")
thrill.filter(s => s.length == 4)
thrill.forall(s => s.endsWith("l"))
thrill.foreach(s => print(s))
thrill.head
thrill.isEmpty
thrill.last
thrill.length

thrill.map(s => s + "y")
thrill.mkString(", ")

thrill.reverse
thrill.sortWith((s, t) => {return s.charAt(0).toLower < t.charAt(0).toLower})

//Use tuples

val pair = (99, "Luftballons")
println(pair._1)
println(pair._2)

val diag3: List[List[Int]] =
  List(
    List(1, 0, 0),
    List(0, 1, 0),
    List(0, 0, 1)
  )

val abcde = List('a', 'b', 'c', 'd', 'e')

abcde.reverse


abcde splitAt 2

abcde.indices zip abcde

val zipped = abcde zip List(1, 2, 3)

//: toString and mkString

abcde.toString

abcde mkString ("[", ",", "]")

val arr = abcde.toArray

arr.toString
arr.toList

//Higher-order methods on class List

val words = List("the", "quick", "brown", "fox")
words map (_.length)

words map (_.toList.reverse.mkString)

List(1, 2, 3, 4, 5) find (_ % 2 == 0)
List(1, 2, 3, 4, 5) find (_ <= 0)

val xss =
  List(List('a', 'b'), List('c'), List('d', 'e'))

xss.flatten

List.concat(List('a', 'b'), List('c'))

var l =List(1,2,3,4,5,6)

 l.map( x => x*2 )



def g(v:Int) = List(v-1, v, v+1)


l.map(x => g(x))

l.flatMap(x => g(x))


val words1 = List("the", "quick", "brown", "fox")

words1 map (_.toList.reverse.mkString)

words map (_.toList)

words flatMap (_.toList)

List(1, 2, 3, -4, 5) takeWhile (_ > 0)


List.range(1, 9, 2)

//COLLECTIONS:

//List buffers

import scala.collection.mutable.ListBuffer

val buf = new ListBuffer[Int]

buf += 1

buf += 2


3 +: buf

//Sets and maps

import scala.collection.mutable

val mutaSet = mutable.Set(1, 2, 3)

val words2 = mutable.Set.empty[String]

for (word <- wordsArray)



words2 += word.toLowerCase

val map = mutable.Map.empty[String, Int]

//Common operations for sets


val

nums = Set(1, 2, 3)

nums + 5

nums - 3

nums ++ List(5, 6)

nums ** Set(1, 3, 5, 7) //intersection

nums.size

nums.contains(3)


nums -- List(1, 2)

import scala.collection.mutable

val words =
  mutable.Set.empty[String]

words += "the"

words -= "the"

words ++= List("do", "re", "mi")

words --= List("do", "re")

words.clear

//count number of words in String

def countWords(text: String) = {
  val counts = mutable.Map.empty[String, Int]
  for (rawWord <- text.split("[ ,!.]+")) {
    val word = rawWord.toLowerCase
    val oldCount =
      if (counts.contains(word)) counts(word)
      else 0
    counts += (word -> (oldCount + 1))
  }
  counts
}


//Common operators on maps


val nums = Map("i" -> 1, "ii" -> 2)

nums + ("vi" -> 6)

nums - "ii"

nums ++ List("iii" -> 3, "v" -> 5)

nums -- List("i", "ii")

nums.size


nums.contains("ii")

nums("ii")

nums.keys

nums.keySet

nums.values

nums.isEmpty

import scala.collection.mutable

val words =
  mutable.Map.empty[String, Int]

words += ("one" -> 1)

words -= "one"

words ++= List("one" -> 1,
  "two" -> 2, "three" -> 3)


words --= List("one", "two")


import scala.collection.immutable.TreeSet

val ts = TreeSet(9, 3, 1, 8, 0, 2, 7, 4, 6, 5)


val cs = TreeSet('f', 'u', 'n')

import scala.collection.immutable.TreeMap

var tm = TreeMap(3 -> 'x', 1 -> 'x', 4 -> 'x')

tm += (2 -> 'x')

var capital = Map("US" -> "Washington", "France" -> "Paris")
capital += ("Japan" -> "Tokyo")
println(capital("France"))


//Queues:

import scala.collection.immutable.Queue

val empty = new Queue[Int]

val has1 = empty.enqueue(1)

val has123 = has1.enqueue(List(2, 3))

val queue = new Queue[String]

import scala.collection.mutable.ArrayBuffer

var fruits = ArrayBuffer[String]()
var ints = ArrayBuffer[Int]()

val x = List(15, 10, 5, 8, 20, 12)

val y = x.groupBy(_ > 10)

val y = x.partition(_ > 10)

val y = x.splitAt(2)

List(10, 5, 8, 1, 7).sorted

val b = List("banana", "pear", "apple", "orange").sorted

List("banana", "pear", "apple", "orange").sortWith(_.length < _.length)










