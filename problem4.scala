import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object problem4 extends App {


    import scala.io.Source

    var a = Source.fromFile("/home/user/Documents/problem4").getLines.toList

  //Filter out rows with null values

    var b = a.filter(q => {var q1 =q.split(","); (q1(0) != "")&&(q1(1) != "") }) //filtering rows with proper data

     for(s <- b)
       {
         var p =s.split(",")
        // println(p(0))
       }

  //Find out total amount spent by each user and sort users based on their spending amount

  var c = b.map(s=>(s.split(",")(0),s.split(",")(1),s.split(",")(2),s.split(",")(3).toInt*s.split(",")(4).toInt))

       println(c)

  var counts = mutable.Map.empty[String, ListBuffer[Int]]

  c.foreach(e => {

    if (counts.contains(e._1.toString)) {

      counts += (e._1 -> ((counts(e._1) += e._4)))
    }
    else {
      var LIST_VALUE = new ListBuffer[Int]()
      LIST_VALUE +=e._4
      counts += ((e._1, LIST_VALUE))
    }
  })

  println(counts)

  //Sorting users by highest spend to lowest spend
  var s =  ListBuffer[(String,Int)]()

  for((k,v)<- counts)
  {
    s+=((k,v.sum))
   s= s.sortWith((p,q)=>p._2>q._2)
  }

   println(s)
}


  //

