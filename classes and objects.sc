//Object Casting

val a = 10

val b = a.asInstanceOf[Long]

val c = a.asInstanceOf[Byte]

//Launching an Application with an Object

//You want to start an application with a main method, or provide the entry point for a
//script.


//define an object
//that extends the App trait

object Hello extends App {
  println("Hello, world")
}

object Hello1 extends App {
  if (args.length == 1)
    println(s"Hello, ${args(0)}")
  else
    println("I didn't get your name.")
}