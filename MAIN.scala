import java.sql.{Connection, DriverManager, ResultSet}


object MAIN extends App {
  println("Postgres connector")

  classOf[org.postgresql.Driver]
  val con_st = "jdbc:postgresql://localhost/postgresondocker?user=postgresondocker&password=postgresondocker"
  val conn = DriverManager.getConnection(con_st)

  try {
    val stm = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)

    val rs = stm.executeQuery("SELECT * from Users")

    while(rs.next) {
      println(rs.getString("username"))
    }
  } finally {
    conn.close()
  }
}

