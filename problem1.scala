import scala.collection.mutable.ListBuffer

object problem1 {


  def main(args: Array[String]) {

    var emp = ListBuffer[String]()
    var sal = ListBuffer[Double]()
    var age = ListBuffer[Int]()

    //TUPLE CONTAINS EMPLOYEE DETAILS WITH AGE,NAME,SALARY and some null values distributed randomly across Tuple
    var a = List("emp1", null, 5000.0, 25, null, 6000.0, "emp2", null, 24, null, null, "emp3", 23, null, 10000.0, null, 21, 7000.0, null, "emp4", null, "emp5", 4000.0, 26);

    for (e <- a) {
      if (e.isInstanceOf[String]) {
        emp += e.asInstanceOf[String]
      }
      else if (e.isInstanceOf[Double]) {
        sal += e.asInstanceOf[Double]
      }

      else if (e.isInstanceOf[Int]) {
        age += e.asInstanceOf[Int]
      }

      else {
       // println("removed null values")
      }

    }


    println("total emp " + emp.length)
    println("total sal " + sal.sum)
    println("avg age  " + age.sum/emp.length)
    println(emp)
  }

}






